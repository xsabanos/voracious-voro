#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include "voro++.hh"

//#include "c:\cygwin64\home\Julia\voro++-0.4.6\src\voro++.hh"

#define VORO_BOX_SIZE 512

const int max_regions=16777216;

enum blocks_mode {
	none,
	length_scale,
	specified
};

void initVoroCompute(std::string src, std::vector<int>& vol) {
	//open src, get pts info, shove into actual compute fction, get volumes back
}

//template<class c_loop,class c_class>
void cmd_line_output(voro::c_loop_all &vl,voro::container &con,const char* format,FILE* outfile,FILE* gnu_file,FILE* povp_file,FILE* povv_file,bool verbose,double &vol,int &vcc,int &tp) {
	int pid,ps=con.ps;double x,y,z,r;
	voro::voronoicell c;
	if(vl.start()) do if(con.compute_cell(c,vl)) {
					vl.pos(pid,x,y,z,r);
					if(outfile!=NULL) c.output_custom(format,pid,x,y,z,r,outfile);
					if(gnu_file!=NULL) c.draw_gnuplot(x,y,z,gnu_file);
					if(povp_file!=NULL) {
						fprintf(povp_file,"// id %d\n",pid);
						if(ps==4) fprintf(povp_file,"sphere{<%g,%g,%g>,%g}\n",x,y,z,r);
						else fprintf(povp_file,"sphere{<%g,%g,%g>,s}\n",x,y,z);
					}
					if(povv_file!=NULL) {
						fprintf(povv_file,"// cell %d\n",pid);
						c.draw_pov(x,y,z,povv_file);
					}
					if(verbose) {vol+=c.volume();vcc++;}
				} while(vl.inc());
}

int main() {
	/*std::vector<int> vol;
	std::string sourceOfPoints = "input.txt";
	voro_compute(sourceOfPoints, vol);


	std::vector<std::vector<std::vector<double>>> fvalues;

	fvalues.resize(VORO_BOX_SIZE);
	for (auto i : VORO_BOX_SIZE) {
		fvalues[i].resize(VORO_BOX_SIZE);
		for (auto j : VORO_BOX_SIZE) {
			fvalues[i][j].resize(VORO_BOX_SIZE);
		}
	}

	for (int i = 0; i < BOX_SIZE; i++) {
		for (int j = 0; j < BOX_SIZE; j++) {
			for (int k = 0; k < BOX_SIZE; k++) {
				fvalues[i][j][k] = getCellVol(i, j, k);
			}
		}
	}
	// map - numerical key, volume.
	// on re-count - save a new map, compare, take out the points where a change has happened, do newVolume (div) oldVolume
	// which yields a double number,
	// we also have a map of numerical key and f-ctional value
	// += save the div result & f-ctional value product somewhere, rinse repeat on the rest of the changed points, output the value/save it somewhere
	*/

	// Points could be a std::pair<std::tuple<int,int,int>, double> = pair of (x,y,z) coords and a value/volume
	// but then they would have to be divvied into vol points vs fvalue points
	// this wouldn't be so bad maybe iff vol points were not being output?
/*
    class PointOfInterest {
        std::tuple<const int, const int, const int> coords;
        double volume;
        double fvalue;

    }
*/

//received: bunch of data to parse into points and fvalues -> knownFvalueMap
//          parse the same bunch of data but this time just take the point coords, call
//                cell constructing & volume counting fction -> build knownVolumeMap;
//          in a generator cycle, generate all the other points that are not the points received,
//                process them in a process fction
//          process fction: generate newPointVolumeMap (just like knownVolumeMap but w/ 1 more key&value pair),
//                  return fvalue of newPoint just like code below does for 1 point

	voro::pre_container *pcon=NULL;

	int nx, ny, nz, init_mem(8);

	double ax= 0.0, bx= 100.0;
	double ay= 0.0, by= 100.0;
	double az= 0.0, bz= 100.0;

	double vol=0;
	int tp=0,vcc=0;

	//endof vars from voro++

	std::map<const int, double> knownVolumeMap;
	std::map<const int, double> newPointVolumeMap;
	std::map<const int, double> knownFvalueMap;

	std::pair<int, double> currPoint;
	currPoint = std::make_pair(6, 0.0); //fvalue, NOT volume!

	/*
	example data
	*/

	knownVolumeMap.insert(std::make_pair(1, 3.4));
	knownVolumeMap.insert(std::make_pair(2, 10.2));
	knownVolumeMap.insert(std::make_pair(3, 1.9));
	knownVolumeMap.insert(std::make_pair(4, 12.1));
	knownVolumeMap.insert(std::make_pair(5, 16.0));

	newPointVolumeMap.insert(std::make_pair(1, 3.4));
	newPointVolumeMap.insert(std::make_pair(2, 9.7)); //stolen 0.5
	newPointVolumeMap.insert(std::make_pair(3, 1.1)); //stolen 0.8
	newPointVolumeMap.insert(std::make_pair(4, 12.1));
	newPointVolumeMap.insert(std::make_pair(5, 15.1));//stolen 0.9
	newPointVolumeMap.insert(std::make_pair(6, 2.2)); //currPoint, but with volume

	knownFvalueMap.insert(std::make_pair(1, 10));
	knownFvalueMap.insert(std::make_pair(2, 70));
	knownFvalueMap.insert(std::make_pair(3, 20));
	knownFvalueMap.insert(std::make_pair(4, 40));
	knownFvalueMap.insert(std::make_pair(5, 30));

	for(auto const& i : knownVolumeMap) {
		if (*newPointVolumeMap.find(i.first) != i) {
			// fugly, but works?
			currPoint.second += (i.second - newPointVolumeMap.find(i.first)->second) / newPointVolumeMap.find(currPoint.first)->second * knownFvalueMap.find(i.first)->second;

			//this is probably wrong:
			//currPoint.second = currPoint.second + ((newPointVolumeMap.find(i.first)->second) / i.second * knownFvalueMap.find(i.first)->second);
		}
	}
	knownFvalueMap.insert(currPoint);

	for(auto const& i : knownFvalueMap) {
		std::cout << i.first << " " << i.second << std::endl;
	}


	pcon=new voro::pre_container(ax,bx,ay,by,az,bz,false,false,false);
	pcon->import("input.txt"); // I import here, not file
	pcon->guess_optimal(nx,ny,nz);

	voro::container con(ax,bx,ay,by,az,bz,nx,ny,nz,false,false,false,init_mem);
	pcon->setup(con);
	delete pcon;
	voro::c_loop_all vla(con);
	const char *c_str= "%i %q %v";
	FILE* testout = fopen("testout.txt", "rw");
	cmd_line_output(vla,con,c_str,testout,nullptr,nullptr,nullptr,false,vol,vcc,tp);
	fclose(testout);
	return 0;
}


